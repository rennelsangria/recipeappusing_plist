//
//  ViewController.m
//  RecipeAppUsing_pList
//
//  Created by Rennel Sangria on 5/31/15.
//  Copyright (c) 2015 Rennel Sangria. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()


@end

@implementation ViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    //button
//        UIButton* btn = [UIButton buttonWithType:UIButtonTypeSystem];
//        [btn setTitle:@"TouchMe" forState:UIControlStateNormal];
//        [btn setFrame:CGRectMake(100,100,100,100)];
//        [btn addTarget:self action:@selector(btnTouched:) forControlEvents:UIControlEventTouchUpInside];
//        [btn setBackgroundColor:[UIColor redColor]];
//        [self.view addSubview:btn];
    
    //NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    //[defaults setObject:@"My String" forKey:@"myString"];
    //[defaults synchronize];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Property List" ofType:@"plist"];
    //Build the array from the plist
    arrayOfRecipes = [[NSArray alloc] initWithContentsOfFile:path];

    
    UITableView* tableView = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStylePlain];
    tableView.delegate = self;
    tableView.dataSource = self;
    
    [self.view addSubview:tableView];
    
}

-(void)btnTouched:(UIButton*)btn{
    //NSLog(@"I was touched");
    InformationViewController* info = [InformationViewController new];
    [self.navigationController pushViewController:info animated:YES];
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [arrayOfRecipes count];
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell* cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell){
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    //cell.textLabel.text=[[arrayOfRecipes objectAtIndex:indexPath.row]valueForKey:@"Name"];
    
    
    NSDictionary* infoDictionary = arrayOfRecipes[indexPath.row];

    
    cell.textLabel.text = [infoDictionary objectForKey:@"Name"];
   
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary* infoDictionary = arrayOfRecipes[indexPath.row];

    InformationViewController* info = [InformationViewController new];
    info.informationDictionary = infoDictionary;
     [self.navigationController pushViewController:info animated:YES];
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
