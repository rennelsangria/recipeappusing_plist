//
//  InfomationViewController.m
//  RecipeAppUsing_pList
//
//  Created by Rennel Sangria on 5/31/15.
//  Copyright (c) 2015 Rennel Sangria. All rights reserved.
//

#import "InformationViewController.h"


@interface InformationViewController ()

@end

@implementation InformationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    
    UILabel* nameLbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 90, self.view.frame.size.width, 30)];
    nameLbl.text = [self.informationDictionary objectForKey:@"Name"];
    nameLbl.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:nameLbl];
    
    UIImageView* iv = [[UIImageView alloc]initWithFrame:CGRectMake(0, 120, self.view.frame.size.width, 300)];
    iv.contentMode = UIViewContentModeScaleAspectFit;
    [iv setImage:[UIImage imageNamed:[self.informationDictionary objectForKey:@"Image"]]];
    [self.view addSubview:iv];
    
    UITextView* txtView = [[UITextView alloc]initWithFrame:CGRectMake(0, 430, self.view.frame.size.width, 300)];
    txtView.textColor = [UIColor brownColor];
    txtView.text = [self.informationDictionary objectForKey:@"Description"];
    [self.view addSubview:txtView];
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
