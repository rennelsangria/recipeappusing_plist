//
//  ViewController.h
//  RecipeAppUsing_pList
//
//  Created by Rennel Sangria on 5/31/15.
//  Copyright (c) 2015 Rennel Sangria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InformationViewController.h"

@interface ViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
{
    NSArray* arrayOfRecipes;
}


@end

