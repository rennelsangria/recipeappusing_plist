//
//  AppDelegate.h
//  RecipeAppUsing_pList
//
//  Created by Rennel Sangria on 5/31/15.
//  Copyright (c) 2015 Rennel Sangria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

